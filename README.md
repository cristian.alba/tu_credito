# Reto Técnico Backend 💻
Aplicación web para gestión de bancos, clientes y créditos, la cual permite:

  - Listar y crear bancos.
  - Listar y crear créditos.
  - Listar, crear, actualizar y eliminar clientes.
---

## Tabla de contenido  📄
1. Requerimientos
2. Instalación
3. URLS
4. Pruebas unitarias
5. Autor

 ---
    
### Requerimientos ⚙️
  - Docker
  - docker-compose

---

### Instalación  🚀

  - Clonar repositorio
    ```sh
        git clone https://gitlab.com/cristian.alba/tu_credito.git
    ```
  - Ejecutar los siguientes comandos:
    ```sh
        docker-compose build
        docker-compose up
    ```
     
---
### URLS 🔩

Las url´s de la aplicación son:

| Acción        | Url      |
|---------------|----------|
| Inicio        | /        |
| Ir a bancos   | banks/   |
| Ir a clientes | clients/ |
| Ir a créditos | credits/ |

---

### Pruebas Unitarias 💣
Para ejecutar las pruebas unitarias:

   ```sh
      docker-compose exec tu_credito pytest
   ```

---

### Autor ✒️

Desarrollado por Cristian Eduardo González Alba

📧 ingcristianalba@gmail.com

📇 <https://www.linkedin.com/in/cristian-alba75/>

🛠️<https://gitlab.com/cristian.alba/tu_credito>