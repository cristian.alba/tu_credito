from django.db import models

# Create your models here.
from django.urls import reverse


class Bank(models.Model):
    """
    Bank Model
    """
    class BankType(models.TextChoices):
        """
        Choices for bank_type field
        """
        GOB = "GOB", "GOBIERNO"
        PRI = "PRI", "PRIVADO"

    name = models.CharField(
        null=False, blank=False,
        max_length=255,
        verbose_name="Nombre",
        help_text="Nombre del banco"
    )
    bank_type = models.CharField(
        null=False, blank=False,
        max_length=3,
        choices=BankType.choices,
        default=BankType.PRI,
        verbose_name="Tipo",
        help_text="Tipo de banco"
    )
    address = models.TextField(
        null=True, blank=True,
        verbose_name="Dirección",
        help_text="Dirección del banco"
    )

    class Meta:
        ordering = ['id']

    def get_bank_type_value(self):
        return self.BankType(self.bank_type).label

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('bnk:list')
