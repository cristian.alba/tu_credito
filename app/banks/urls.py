from django.urls import path

from banks.views import BankList, BankCreate

app_name = 'bnk'

urlpatterns = [
    path('list/', BankList.as_view(), name='list'),
    path('create/', BankCreate.as_view(), name='create'),
]
