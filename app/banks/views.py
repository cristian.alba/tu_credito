
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse
from django.views.generic import ListView
from django.views.generic.edit import CreateView

from banks.models import Bank


# Create your views here.

class BankList(ListView):
    """
    View for list Banks
    """
    model = Bank
    template_name = 'banks/list.html'


class BankCreate(SuccessMessageMixin, CreateView):
    """
    View for create Banks
    """
    model = Bank
    form = Bank
    fields = "__all__"
    template_name = 'banks/create.html'
    success_message = 'Banco creado exitosamente!!!'

    # Redirection after save
    def get_success_url(self):
        return reverse('bnk:list')
