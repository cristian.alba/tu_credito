
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse
from django.views.generic import ListView
from django.views.generic.edit import CreateView

from credits.models import Credit


# Create your views here.

class CreditList(ListView):
    """
    View for list Credit
    """
    model = Credit
    template_name = 'credits/list.html'


class CreditCreate(SuccessMessageMixin, CreateView):
    """
    View for create Credits
    """
    model = Credit
    form = Credit
    fields = "__all__"
    template_name = 'credits/create.html'
    success_message = 'Crédito creado exitosamente!!!'

    # Redirection after save
    def get_success_url(self):
        return reverse('cre:list')
