from django.urls import path

from credits.views import CreditList, CreditCreate

app_name = 'cre'

urlpatterns = [
    path('list/', CreditList.as_view(), name='list'),
    path('create/', CreditCreate.as_view(), name='create'),
]
