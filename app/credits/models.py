from django.db import models

from clients.models import Client
from banks.models import Bank
# Create your models here.


class Credit(models.Model):
    """
    Credit model
    """

    class CreditType(models.TextChoices):
        """
        Choices for credit_type field
        """
        AUT = "AUT", "AUTOMOTRIZ"
        HIP = "HIP", "HIPOTECARIO"
        COM = "COM", "COMERCIAL"

    client = models.ForeignKey(
        Client, on_delete=models.PROTECT,
        verbose_name="Cliente",
        help_text="Cliente del credito"
    )
    description = models.TextField(
        null=False, blank=False,
        verbose_name="Descripción",
        help_text="Descripción del crédito"
    )
    minimum_payment = models.DecimalField(
        null=False, blank=False,
        max_digits=20,
        decimal_places=2,
        verbose_name="Pago mínimo",
        help_text="Pago mínimo del crédito"
    )
    maximum_payment = models.DecimalField(
        null=False, blank=False,
        max_digits=20,
        decimal_places=2,
        verbose_name="Pago máximo",
        help_text="Pago máximo del crédito"
    )
    term_months = models.PositiveIntegerField(
        null=False, blank=False,
        verbose_name="Plazo en meses",
        help_text="Plazo en meses del crédito"
    )
    register_date = models.DateField(
        null=False, blank=False, auto_now_add=True,
        verbose_name="Fecha de registro",
        help_text="Fecha de registro del crédito"
    )
    bank = models.ForeignKey(
        Bank, on_delete=models.PROTECT,
        verbose_name="Banco",
        help_text="Banco que otorga el crédito"
    )
    credit_type = models.CharField(
        null=True, blank=True,
        max_length=3,
        choices=CreditType.choices,
        verbose_name="Tipo",
        help_text="Tipo de crédito"
    )

    class Meta:
        ordering = ['id']

    def get_credit_type_value(self):
        return self.CreditType(self.credit_type).label

    def __str__(self):
        return f" {self.client.name} | {self.credit_type.name}"
