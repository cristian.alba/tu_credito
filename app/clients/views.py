
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib import messages

from clients.models import Client


# Create your views here.

class ClientList(ListView):
    """
    View for list Clients
    """
    model = Client
    template_name = 'clients/list.html'


class ClientDetail(DetailView):
    """
    View for detail Clients
    """
    model = Client
    template_name = 'clients/detail.html'


class ClientCreate(SuccessMessageMixin, CreateView):
    """
    View for create Clients
    """
    model = Client
    form = Client
    fields = "__all__"
    template_name = 'clients/create.html'
    success_message = 'Cliente creado exitosamente!!!'

    # Redirection after save
    def get_success_url(self):
        return reverse('cli:list')


class ClientUpdate(SuccessMessageMixin, UpdateView):
    model = Client
    form = Client
    fields = "__all__"
    template_name = 'clients/update.html'
    success_message = 'Cliente actualizado correctamente !!!'

    # Redirection after update
    def get_success_url(self):
        return reverse('cli:list')


class ClientDelete(SuccessMessageMixin, DeleteView):
    model = Client
    form = Client
    fields = "__all__"

    # Redirection after delete
    def get_success_url(self):
        success_message = 'Cliente eliminado correctamente !'
        messages.success(self.request, success_message)
        return reverse('cli:list')
