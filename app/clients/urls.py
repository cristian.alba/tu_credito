from django.urls import path

from clients.views import ClientList, ClientCreate, ClientDetail, ClientUpdate, ClientDelete

app_name = 'cli'

urlpatterns = [
    path('list/', ClientList.as_view(), name='list'),
    path('create/', ClientCreate.as_view(), name='create'),
    path('detail/<int:pk>', ClientDetail.as_view(), name='detail'),
    path('update/<int:pk>', ClientUpdate.as_view(), name='update'),
    path('delete/<int:pk>', ClientDelete.as_view(), name='delete'),

]
