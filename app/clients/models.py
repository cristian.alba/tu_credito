from django.db import models
from django import forms
from django.urls import reverse

from banks.models import Bank
# Create your models here.


class Client(models.Model):
    """
    Client model
    """

    class PersonType(models.TextChoices):
        """
        Choices for person_type field
        """
        NAT = "NAT", "NATURAL"
        JUR = "JUR", "JURIDICA"

    name = models.CharField(
        null=False, blank=False,
        max_length=200,
        verbose_name="Nombre(s)",
        help_text="Nombre(s) del cliente"
    )
    last_name = models.CharField(
        null=False, blank=False,
        max_length=200,
        verbose_name="Apellido(s)",
        help_text="Apellido(s) del cliente"
    )
    birth_date = models.DateField(
        null=False, blank=False,
        verbose_name="Fecha de nacimiento",
        help_text="Fecha de nacimiento del cliente"
    )
    age = models.PositiveIntegerField(
        null=True, blank=True,
        verbose_name="Edad",
        help_text="Edad del cliente"
    )
    nationality = models.CharField(
        null=True, blank=True,
        max_length=255,
        verbose_name="Nacionalidad",
        help_text="Nacionalidad del cliente"
    )
    room_address = models.TextField(
        null=True, blank=True,
        verbose_name="Dirección de habitación",
        help_text="Dirección de habitación del cliente"
    )
    email = models.CharField(
        null=False, blank=False,
        max_length=255,
        verbose_name="Correo",
        help_text="Correo del cliente"
    )
    cellphone = models.CharField(
        null=True, blank=True,
        max_length=50,
        verbose_name="Teléfono",
        help_text="Teléfono del cliente"
    )
    person_type = models.CharField(
        null=True, blank=True,
        max_length=3,
        choices=PersonType.choices,
        verbose_name="Tipo",
        help_text="Tipo de persona"
    )
    bank = models.ForeignKey(
        Bank, on_delete=models.PROTECT,
        verbose_name="Banco",
        help_text="Banco del cliente"
    )

    class Meta:
        ordering = ['id']

    def get_person_type_value(self):
        return self.PersonType(self.person_type).label

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('cli:list')
