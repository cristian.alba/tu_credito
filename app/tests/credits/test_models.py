import pytest

from banks.models import Bank
from clients.models import Client
from credits.models import Credit


@pytest.fixture
def bank_factory():
    """
    Return Bank instance.
    """
    bank = Bank(
        name="Banco A",
        bank_type=Bank.BankType.PRI,
        address="Calle 123"
    )
    bank.save()

    return bank


@pytest.fixture
def client_factory(bank_factory):
    """
    Return Client instance.
    """
    client = Client(
        name="María",
        last_name="Reyes",
        birth_date="2022-07-14",
        age=31,
        nationality="Colombiana",
        room_address="Calle 123",
        email="maria@gmail.com",
        cellphone="1234567899",
        person_type=Client.PersonType.NAT,
        bank=bank_factory
    )
    client.save()

    return client


@pytest.mark.django_db
def test_client_model(client_factory):
    """
    Tests if model at works as expected.
    """
    client = client_factory
    credit = Credit(
        client=client,
        description="Crédito 1",
        minimum_payment=2,
        maximum_payment=5,
        term_months=15,
        register_date="2022-06-13",
        bank=client.bank,
        credit_type=Credit.CreditType.AUT,
    )
    credit.save()

    assert isinstance(credit, Credit)
    assert isinstance(credit.client, Client)
    assert credit.description == "Crédito 1"

