import pytest

from banks.models import Bank
from clients.models import Client


@pytest.fixture
def bank_factory():
    """
    Return Bank instance.
    """
    bank = Bank(
        name="Banco A",
        bank_type=Bank.BankType.PRI,
        address="Calle 123"
    )
    bank.save()

    return bank


@pytest.mark.django_db
def test_client_model(bank_factory):
    """
    Tests if model at works as expected.
    """
    client = Client(
        name="María",
        last_name="Reyes",
        birth_date="1990-06-13",
        age=32,
        nationality="Colombiana",
        room_address="Calle 123",
        email="maria@gmail.com",
        cellphone="1234567899",
        person_type=Client.PersonType.NAT,
        bank=bank_factory
    )
    client.save()

    assert isinstance(client, Client)
    assert client.name == "María"
    assert client.last_name == "Reyes"
