import pytest

from banks.models import Bank


@pytest.mark.django_db
def test_bank_model():
    """
    Tests if model at works as expected.
    """
    bank = Bank(
        name="Banco 1",
        bank_type=Bank.BankType.PRI,
        address="Calle 123"
    )
    bank.save()

    assert isinstance(bank, Bank)
    assert bank.name == "Banco 1"
