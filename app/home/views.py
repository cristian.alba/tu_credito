from django.shortcuts import render

# Create your views here.


def home(request):
    """
    Return render home page.
    """
    return render(request, 'home.html', context={})
